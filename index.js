/*
	1. In the S26 folder, create an a1 folder and an activity.js file inside of it.
	2. Copy the questions provided by your instructor into the activity.js file.
	3. The questions are as follows:
	
*/

// - What directive is used by Node.js in loading the modules it needs?
	// Answer: "require" directive

// - What Node.js module contains a method for server creation?
	// Answer: http module

// - What is the method of the http object responsible for creating a server using Node.js?
	// Answer: createServer() method

// - What method of the response object allows us to set status codes and content types?
	// Answer: writeHead() method

// - Where will console.log() output its contents when run in Node.js?
	// Answer: Terminal

// - What property of the request object contains the address's endpoint?
	// Answer: URL / Browser


/*
	4. Create an index.js file inside of the activity folder.
*/


// 5. Import the http module using the required directive.
const http = require("http");

// 6. Create a variable port and assign it with the value of 3000.
const port = 3000;

// 7. Create a server using the createServer method that will listen in to the port provided above.
http.createServer((request, response) => {

// 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to the login page.");
	}
	// All other routes will return a message of "Page not available"
	else{
		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
}).listen(port);

// 8. Console log in the terminal a message when the server is successfully running.
console.log(`Server is successfully running: ${port}`);

/*
	5. Import the http module using the required directive.
	6. Create a variable port and assign it with the value of 3000.
	7. Create a server using the createServer method that will listen in to the port provided above.
	8. Console log in the terminal a message when the server is successfully running.
	9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	10. Access the login route to test if it’s working as intended.
	11. Create a condition for any other routes that will return an error message.
	12. Access any other route to test if it’s working as intended.
	13. Create a git repository named S26.
	14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	15. Add the link in Boodle.
*/